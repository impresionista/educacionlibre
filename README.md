# EducacionLibre

La intención de este repositorio es recopilar software útil para docencia e investigación. La intención es priorizar Software y proyectos Libres (ver https://creativecommons.org/ y https://www.fsf.org/ ) para ayudar a garantizar el acceso a la educación a la mayor cantidad de personas posibles y para garantizar el entendimiento completo de las herramientas que se usan.

Toda la información se va organizando en la [Wiki](https://gitlab.com/impresionista/educacionlibre/-/wikis/EducacionLibre) de este repositorio:

https://gitlab.com/impresionista/educacionlibre/-/wikis/EducacionLibre